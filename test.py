from datetime import datetime
import requests
import json
from requests.packages import urllib3
urllib3.disable_warnings()
from glob import glob

def get_token():
    url = "https://210.245.0.226/rest/login"
    payload = {"username": "ftn-cfg", "password": "kthtinfhn"}
    response = requests.post(url, data=payload, verify=False)
    token = json.loads(response.content)["token"]
    return token


def get_devices_opsview():
    token = get_token()
    url = 'https://210.245.0.226/rest/status/service?hostgroupid=1204&host_filter=handled&host_state=0'

    dict_data = dict()

    headers = {
        "Content-Type": "application/json",
        "X-Opsview-Username": 'ftn-cfg',
        "X-Opsview-Token": token,
    }

    request = requests.get(url, headers=headers, verify=False)

    items = json.loads(request.text).get('list')

    list_ip = []
    list_hostname = []
    list_branch = []

    for item in items:
        try:
            info = item.get('name').replace('INF-', '').split('-')
            ip = info[2]
            hostname = info[1]
            branch = info[0]
            
            if 'PWDA' not in hostname:
                list_ip.append(ip)
                list_hostname.append(hostname)
                list_branch.append(branch)
        except:
            print(item.get('name'))
    return list_ip, list_hostname, list_branch



def get_list_ip_trap():
    list_path_file = glob('*.trap')
    list_ip = []
    for path_file in list_path_file:
        ip = path_file.replace('.trap', '')
        list_ip.append(ip)

    return list_ip


ip_trap = get_list_ip_trap()
ip_ops, list_hostname, list_branch = get_devices_opsview()

list_ip_nok = list(set(ip_ops) - set(ip_trap))
for ip in list_ip_nok:
    index = ip_ops.index(ip)
    hostname = list_hostname[index]
    print(ip, hostname)