import os
import re
import json
from elasticsearch import Elasticsearch
import time
import arrow
import pandas as pd
from datetime import datetime, date, timedelta



KIBANA_HOST = '172.30.206.248'
KIBANA_PORT = '9200'
KIBANA_USER = 'monitor'
KIBANA_SECRET = 'ftel@mon2019'


def connect_elastic():
    """
    Connect to Elastic search
    """
    es = None
    try:
        es = Elasticsearch('{}:{}'.format(KIBANA_HOST, KIBANA_PORT),
                           timeout=10,
                           http_auth=(KIBANA_USER, KIBANA_SECRET))
    except Exception as e:
        print("Error while connecting to Elasticsearch", e)
    return es



indexs = ["nops-*"]



def get_logs(start_time, host, keyword):
    es = connect_elastic()
    start_time_convert = datetime.strptime(
        start_time, '%Y-%m-%dT%H:%M:%S.%fZ')  # .strftime('%Y-%m-%dT%H:%M:%SZ')
    end_time = (start_time_convert + timedelta(hours=15)
                ).strftime('%Y-%m-%dT%H:%M:%SZ')

    query = {
        "query": {
            "bool": {
                "must": [
                    {
                        "range": {
                            "@timestamp": {
                                "gte": start_time,
                                "lte": end_time
                            }
                        }
                    },
                    {
                        "match_phrase": {
                            "nagios_hostname": host
                        }
                    },
                    {
                        "match_phrase": {
                            "nagios_message": keyword
                        }
                    }
                ]
            }
        },
        "sort": [
            {
                "@timestamp": "asc"
            }
        ]
    }

    logs = es.search(index=indexs, size=10000, body=query, scroll='2m')

    sid_bb = logs['_scroll_id']
    scroll_size_bb = len(logs['hits']['hits'])

    return_data = []
    while scroll_size_bb > 0:
        for item in logs['hits']['hits']:
            timestamp = item.get('_source').get('@timestamp')
            # item.get('_source')['@timestamp'] = str(arrow.get(timestamp).to('Asia/Saigon'))
            return_data.append(item.get('_source'))

        logs = es.scroll(scroll_id=sid_bb, scroll='2m')

        sid_bb = logs['_scroll_id']

        scroll_size_bb = len(logs['hits']['hits'])
    return return_data


def get_logs_2(ip):
    es = connect_elastic()
    query = {
        "query": {
            "bool": {
                "must": [
                    {
                        "range": {
                            "@timestamp": {
                                "gte": "now-90d",
                                "lte": "now"
                            }
                        }
                    },
                    {
                        "match_phrase": {
                            "message": "INF_Power_Lost"
                        }
                    },
                    {
                        "match_phrase": {
                            "message": "SOFT"
                        }
                    },
                    {
                        "match_phrase": {
                            "message": ip
                        }
                    },
                    {
                        "match_phrase": {
                            "message": "Battery-Test"
                        }
                    }
                ]
            }
        },
        "sort": [
            {
                "@timestamp": "asc"
            }
        ]
    }


    logs = es.search(index=indexs, size=10000, body=query, scroll='2m')

    sid_bb = logs['_scroll_id']
    scroll_size_bb = len(logs['hits']['hits'])

    return_data = []
    while scroll_size_bb > 0:
        for item in logs['hits']['hits']:
            timestamp = item.get('_source').get('@timestamp')
            # item.get('_source')['@timestamp'] = str(arrow.get(timestamp).to('Asia/Saigon'))
            return_data.append(item.get('_source'))

        logs = es.scroll(scroll_id=sid_bb, scroll='2m')

        sid_bb = logs['_scroll_id']

        scroll_size_bb = len(logs['hits']['hits'])
    return return_data



def get_battery_test():
    keyword = 'DANG CO DIEN LUOI AC'

    logs = get_logs_2()

    last_sub_date = 0
    date_test = ''

    for log in logs:
        batt_date = log.get('@timestamp')
        # print(batt_date)
        text = log.get('message')
        ip_hostname_branch = text.rsplit('ALERT: ')[1].rsplit(
            ';3.INF_Power_Lost')[0].replace('INF-', '')

        if 'HCM' not in ip_hostname_branch:
            split_text = ip_hostname_branch.split('-')
            cnt = split_text[0]
            hostname = split_text[1]
            ip = split_text[2]

            log1 = get_logs(batt_date, ip, keyword)
            if log1 != []:
                log1 = log1[0]
                ac_date = log1.get('@timestamp')

                subtract_date = round(
                    ((arrow.get(ac_date).timestamp - arrow.get(batt_date).timestamp) / 3600), 2)

                if last_sub_date < subtract_date:
                    last_sub_date = subtract_date
                    date_test = str(arrow.get(batt_date).to('Asia/Saigon'))

    return date_test, last_sub_date

