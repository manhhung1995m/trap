import sys
import telebot
from puresnmp import get
import time
from datetime import datetime
import pymongo
from dangcodienluoiac import get_battery_test


parameters = sys.argv[1:]
ip = parameters[0]
dc = parameters[1]


TOKEN = "1370218607:AAGrZxv3l21TjYgzO3vzoDLvOrfk_brNUcw"
bot = telebot.TeleBot(token=TOKEN)
chat_id = -399293972

date_test, time_test = get_battery_test(ip)

ur = ''
ib = ''
try:
    ur = get(ip, 'public', '.1.3.6.1.4.1.21940.2.4.2.1.0', timeout=1).decode('ascii')
    ib = get(ip, 'public', '.1.3.6.1.4.1.21940.2.4.2.14.0',
             timeout=1).decode('ascii')
except:
    pass

msg = '''
    <b>Kết quả test ắc quy gần nhất:</b>
    Nguồn/IP: {}
    Ngày test: {}
    Thời gian test: {}
    Điện áp cuối: ''
    Điện áp hiện tại (Ur): {}
    Dòng ắc quy (Ib): {}
'''.format(ip, date_test, str(time_test), str(ur), str(ib))
bot.send_message(chat_id, msg, parse_mode='HTML')



def get_last_test(ip):
    test_date = 'None'
    time_test = 'None'
    dc_test = None
    with open(ip +'.trap') as f:
        list_line = f.readlines()
        index_batt_on = 0
        index_batt_off = 0
        dc_ouput = None
        time_stamp_off = 0

        for i in range(len(list_line)):
            if i >= index_batt_off :
                line = list_line[i]
                if 'Battery-Test On' in line:
                    batt_on = line.split(' |-| ')[0]
                    index_batt_on = list_line.index(line)
                    print(batt_on)
                    time_stamp_on = time.mktime(datetime.strptime(
                        batt_on, '%a %b %d %Y %H:%M:%S').timetuple())

                    for j in range(index_batt_on, len(list_line)):
                        line_off = list_line[j]
                        if 'Battery-Test Off' in line_off:
                            split_line_off = line_off.split(' |-| ')
                            batt_off = split_line_off[0]
                            print(batt_off)
                            dc_ouput = float(split_line_off[3].replace(' ', '').replace('\n', ''))/1000
                            print(dc_ouput)
                            index_batt_off = list_line.index(line_off)
                            time_stamp_off = time.mktime(datetime.strptime(
                                batt_off, '%a %b %d %Y %H:%M:%S').timetuple())
                            print(index_batt_off)
                            break

                    sub_time = (time_stamp_off - time_stamp_on)/60
                    
                    if sub_time > 3600:
                        test_date = batt_on
                        time_test = sub_time
                        dc_test = dc_ouput
                # print(list_line)
            # print(line)
    return test_date, time_test, dc_ouput

